## The Basics

### Define your DAG: the right way

- creating dag in dag folder - scheduler will parse file only in there is `dag` or `airflow` in the file
- option to modify this is in env variable `DAG_DISCOVERY_SAFE_MODE` under the section core in the airflow config (not recommended)
- `.airflowignore` using this file there  is option to configure which type of file can be ignored in airflow

- context manager notation is better because there is no need for passing `dag=dag` to every operator:
```python
with DAG(
    dag_id="my_dag", 
    description="This is my DAG doing some displays",
    start_date=datetime(2021, 1, 1),
    schedule_interval="@daily",
    dagrun_timeout=timedelta(minutes=10),
    tags=["astronomer"],
    catchup=False,
    max_active_runs=1,
) as dag:
```

- `dag_id` - unique identifier across airflow - if there are 2 DAG with the same `dag_id` airflow won't display an error it will randomly show one or second DAG in UI
- `description` - short info about what DAG is doing
- `start_date`:
    - not mandatory in DAG
    - `start_date` - has to be specified in the task, but it's more common to use it in the DAG itself
    - each operator can have its own `start_date`
- `schedule_interval` - defines whet DAG is scheduled:
    - `@daily` (default)
    - crontab
    - timedelta
- `dagrun_timeout` - max time that DAG is executed
- `tags` - tags used for filtering projects
- `catchup`:
    - can be modified in `CATCHUP_BY_DEFAULT`
    - by default airflow will run all non triggered DAGs (`start_date` 10 years ago it will schedule all those runs till now)
- `airflow dags backfill` - commend for backfilling the DAGs

### DAG Scheduling 101

`start_date` - Date at which tasks start being scheduled
`schedule_interval` - interval of time from the min(start_date) as which the DAG is triggered

The DAG [X] starts being scheduled from the start_date and will be triggered after every schedule_interval


Ex: Assuming a start date at 10:00 AM and schedule interval every 10-minute dags schedule dag is actually run after the scheduled interval(start_date), but the execution_date has the time before

![image.png](./media/102.png)

### Cron vs Timedelta

- `cron` expresion is stateless
    - `"@daily"` -> `"0 0 * * *"` - not waiting 24h between `start_date` and `execition_date`
        - 01/01 00:00
        - 01/02 00:00
        - ...
- `timedelta` expresion is statefull, relative - (e.g every 3 days)
    - trigering DAG based on latest `execution_date`
        -  01/01 10:00
        -  02/01 10:00
        - ...

### Task idempotence and determinism

- determinism - for the same input gives the same output
- idempotence - task executed multiple times gives the same result

Tasks should be written that way to execute them more than once:

 - SQL - `CREATE TABLE IF NOT EXISTS...` (`CREATE TABLE` won't work)
 - bash - `mkdir -p my_folder` (`mkdir my_folder`` won't work)

### Backfilling

- if `CATCHUP_BY_DEFAULT` is set to `False` there is still an option to run a backfill
- if `catchup=True` - will trigger all dag runs from the `start_date` to the current date
- `airflow dags backfill -s 2020-01-01 -e 2021-01-01 dag_name`
- `max_active_runs=1` - avoid to run more than 1 dag at a time
- in the interface can rerun by clearing the selected dag runs

## Master your Variables

### Variables

Airflow -> Admin -> Variables -> Add Variable [Key, Val, Description] - prefix with DAG name to know to which DAG it's connected to
```python
from airflow.models import Variable

def _extract():
    partner = Variable.get("my_dag_partner")
```
It's creating a connection to the database

Testing tasks in Terminal:
```bash
airflow tasks test my_dag extract 2021-01-01
```

To hide the value of the variable use keywords like `‘access_token’, ‘api_key’, ‘apikey’,’authorization’, ‘passphrase’, ‘passwd’, ‘password’, ‘private_key’, ‘secret’, ‘token’`

or modify variable in Airflow configuration:

```
[core]
sensitive_var_conn_names = comma,separated,sensitive,names
```

### Properly fetch your Variables

- fetching variable out of scope of function will create a connection to Airflow DB each time DAG is parsed by Airflow (every 30 seconds)
- a couple of variables can be packed to give only one connection to DB by using `.json`

Airflow -> Admin -> Variables -> Add Varialbe [Key, Val, Description]
- Key: "my_dag_partner"
- Val: 
```json  
{"name": "partner_a", "api_secret": "mysecret", "path": "/tmp/partner_a"}
```
- Description: "Variable for partners"

```python
def _extract():
    partner_settings = Variable.get("my_dag_partner_secret", deserialize_json=True)
    name = partner_settings['name']
    api_key = partner_settings['api_secret']
    path = partner_settings['path']
```

**Note! - this way is creating a connection to Airflow DB each parsing! - don't do it**: 

```python
def _extract(partner_name):
   print(partner_name)

extract = PythonOperator(
    task_id="extract",
    python_callable=_extract,
    op_args=[Variable.get("my_dag_partner_secret", deserialize_json=True)['name']]
)
```

This one is working ok - variable is fetched only once when DAG is running (template engine):

```python
def _extract(partner_name):
   print(partner_name)

extract = PythonOperator(
    task_id="extract",
    python_callable=_extract,
    op_args=["{{ var.json_my.my_dag_partner_secret.name }}"]
)
```

- Template engine can be also used in bash

### The Power of Environment Variables

Adding ENV variables for Airflow:
- In the Dockerfile:
`ENV AIRFLOW_VAR_VARIABLE_NAME_1='{"key1":"value1", "key2":"value2"}'`
- Reseting Airflow environment: `astro dev stop && astro dev start`
- Showing Airflow processes: `airflow ps`
- Accessing Scheduler: `docker exec -it CONTAINER_ID /bin/bash`
- Listing Airflow Variables: `airflow variables list`
- Showing environmental variable added to docker: `env | grep AIRFLOW_VAR`

Adding environmental variables is good because:
- fewer connections to DB
- sensitive values are not shown

6 different ways of creating variables:
- Airflow UI   👌
- Airflow CLI 👌
- REST API 👌
- Environment Variables ❤️
- Secret Backend ❤️
- Programatically 😖

Notice that it is possible to create connections with environment variables. You just have to export the env variable:

`AIRFLOW_CONN_NAME_OF_CONNECTION=your_connection`

More secure ways of storing variables is by using [Secret Backend](https://airflow.apache.org/docs/apache-airflow/stable/security/secrets/secrets-backend/index.html).

Order in which Airflow checks if your variable/connection exists:

![image.png](./media/103.png)

## The power of the TaskFlow API

### Add data at runtime with templating

- Templating:
```python
"{{ var.json_my.my_dag_partner_secret.name }}"
```

- info about Airflow: https://registry.astronomer.io/
- for PythonOperator they are: `templates_dict`, `op_args` and `op_kwargs`
- for PostgresOperator they are `sql` and the `.sql` extension 
- syntax: param: `'{{ templated_param }}'` or param: `'path/to_file.ext'`
- templated params are defined in the Operator [template_fields](https://github.com/apache/airflow/blob/main/airflow/operators/python.py#L134), [template_ext](https://github.com/apache/airflow/blob/main/airflow/providers/postgres/operators/postgres.py#L46)

- adding templates for operators:
```python
class CustomPostgressOperator(PostgresOperator):
    template_fields = ('sql', 'parameters')
```

- parameters can be found in Airflow -> DAG -> Task Instance Run -> Rendered template

### Sharing data with XCOMs and limitations

- sharing data between tasks
- push XCOM -> store in metadata database -> pull XCOM
- in order to acccess task instance object:
    - value is serialize as `.json` object
    - ti - task_instance
```
def _extract(ti):
    partner_name = "netflix"
    ti.xcom_push(key="partner_name, value=partner_name)

def _process(ti):
    partner_name = ti.xcom_pull(key=partner_name, task_ids="extract")
```
Limitations:
- SQLLite - 2GB per xcom
- PostgreSQL - 1GB per xcom
- MySQL - 64 KB per xcom
- can be increased with XCOM Backends (eg. S3)
    - but you still need to pull data and use memory
    - or use references instead of actual data
- before 2.0 XCOM serialization was done default by picling
- after 2.0 `enable_xcom_pickling = False` default serialization is JSON to avoid Remote Code Executions attacks

Other way of passing value to another task:
```python
def _extract(ti):
    partner_name = "netflix"
    return partner_name

def _process(ti):
    # 1 way
    partner_name = ti.xcom_pull(key=return_value, task_ids="extract")
    # 2 way
    partner_name = ti.xcom_pull(task_ids="extract")
```

Passing multiple xcom's at once by returning json value:
```python
def _extract(ti):
    partner_name = "netflix"
    partner_path = "/partners/netflix"
    return {"partner_name": partner_name, "partner_path": partner_path}

def _process(ti):
    partner_settings = ti.xcom_pull(task_ids="extract")
    print(partner_settings["partner_name"])
```

XComs are avaiable under: Airflow -> Admin -> XComs

### The new way of creating DAGs

TaskFlow API's:

- Decorators - easier and faster way
    - `@task.python`
    - `@task.virtualenv`
    - `@task_group`
    - `@dag`
    - decorators should be added in the front of the function
- XCom ARGS - share data between tasks not having to call `xcom.push`, `xcom.pull`
    - automatically create explicit dependencies
    - returned params can be directly used and they will be passed through XComs

In tasks usage:
- function can't have `_` at the begginign to work
- the task name will be the function name
- the dag id will be the function name adnotated with `@dag`
```python
@task.python
def extract():
    partner_name = "netflix"
    partner_path = "/partners/netflix"
    return {"partner_name": partner_name, "partner_path": partner_path}

extract() >> process
```
In DAG usage:
```python
@dag(
    dag_id="my_dag_decorators", 
    description="This is my DAG doing some display",
    start_date=datetime(2021, 1, 1),
    schedule_interval="@daily",
    dagrun_timeout=timedelta(minutes=10),
    tags=["astronomer"],
    catchup=False,
    max_active_runs=1,
)
def my_dag_decorators():
    # 1way
    extract() >> process()
    # 2way - by passing another function - automatically created dependancy
    process(extract())

my_dag_decorators()
```

### XComs with the TaskFlow API

- `multiple_outputs=True` - will return multiple variables instead of json (multiple task instead of one); will also push dictionary
- `do_xcom_push=False` - prevent  to push json dictionary automatically
- `Dict[str, str]` - second way
```python
@task.python(task_id="extract_partners", do_xcom_push=False, multiple_outputs=True)
def extract() -> Dict[str, str]:
    partner_name = "netflix"
    partner_path = '/partners/netflix/'
    return {"partner_name": partner_name, "partner_path": partner_path}
```

## Grouping your tasks

### SubDAGs: The Hard Way of Grouping your Task

SubDag - dag within a dag:
- pretty complicated, needs some specific associations
- behind the scene it is a Sensor in Airflow 2.0, so it waits for the tasks to complete,
```python
@task.python
def process_a(): 
    ti = get_current_context()['ti']
    partner_name = ti.xcom_pull(key='partner_name', task_ids='extract_partners', dag_id='my_dag_subdag')
    partner_path = ti.xcom_pull(key='partner_path', task_ids='extract_partners', dag_id='my_dag_subdag')
    print(partner_name)   
    print(partner_path) 

def subdag_factory(parent_dag_id, subdag_dag_id, default_args):
    with DAG(f"{parent_dag_id}.{subdag_dag_id}", default_args=default_args) as dag:

        process_a()

    return dag
```

SubDag operator:
- can use `poke_interval`
- can use `mode='reschedule'` 
- have to put `task_concurrency` within each task in subdag
- `default_args` has to be the same for DAG and SubDag
- `subdag_dag_id` has to be the same as task_id of SubDag
- `get_current_context()['ti']`- gives option to pass values from DAG to SubDag

```python
 process_tasks = SubDagOperator(
        task_id="process_tasks",
        subdag=subdag_factory("my_dag_subdag", "process_tasks", default_args)
    )
```

### TaskGroups: The Best Way of Grouping your Tasks

TaskGroups can visually group tasks in Airflow UI

- can import a task group from other file
- easy to use
- can create multiple task groups - one in another
- `@task_group(group_id='process_tasks')` - decorator for creating task groups
```python
def process_tasks(partner_settings):
    with TaskGroup(group_id='process_tasks') as process_tasks:
        process_a(partner_settings['partner_name'], partner_settings['partner_path'])

    return process_tasks
```

## Advanced Concepts

### The (not so) dynamic tasks

- can create dynamic tasks only if Airflow knows the parameters beforehand (in advance)
- can't create tasks based on other tasks output (tasks, SQL queries)
- can create tasks based on a dictionary or a variable or even on some connection in the database
- `add_suffix_on_collision=True` - option to add suffix for duplicated task_id's

### Make your choices with Branching

- Branch Operators:
    - BranchPythonOperator
    - BranchSqlOperator
    - BranchDateTimeOperator
    - BranchDayOfWeekOperator
- you always need to return a `task_id`
- for missing days, you could use a DummyOperator
- for skipped tasks with one parent that run you can use `trigger_rule='none_failed_or_skipped'` to prevent skip
- by default `trigger_rule='all_success'` - due to this if not all tasks are a success, the task won't be triggered

### Change task execution with Trigger Rules

- `all_success` - triggered if all parents succeded
- `all_failed` - triggered if all parents failed (eg. email notification)
- `all_done` - all of the parents are done, no matter if they failed or succeeded
- `one_failed` - as soon as one parent failed
- `one_success` - as soon as one parent success
- `none_failed` - run if no parent task failed or has the status upstream_failed (one of its parents failed)
- `none_skiped` - run if none of the parents were skipped
- `none_failed_or_skipped` - trigger if at least one of the parents has succeeded and all of the parents have been completed
- `dummy` - your task gets trigger immediately

### Dependencies and Helpers

Old way:
- `t2.set_upstream(t1)`
- `t1.set_downstream(t2)`

New way:

- `t1 >> t2`
- `t1 << t2`
- `[t1, t2, t3] >> t5`
- `[t1, t2, t3] >> [t4, t5, t6]` - it doesn't work; error: you can't trade dependencies between two lists
- `[t1, t2, t3] >> t4` ; `[t1, t2, t3] >> t5`; `[t1, t2, t3] >> t6` - passing one by one works
- Cross dependencies:
    - cross_downstream returns nothing 
```python
from airflow.models.baseoperator import cross_downstream, chain

cross_downstream([t1, t2, t3], [t4, t5, t6])
[t4, t5, t6] >> t7
```
- Chain dependencies:
    - lists need to have the same size
```python
from airflow.models.baseoperator import cross_downstream, chain

chain(t1, [t2, t3], [t4, t5], t6])
```
- Mixing also possible:
```python
cross_downstream([t2, t3], [t4, t5])
chain(t1, t2, t5, t6)
```

### Get the control of your tasks

Concurrency - at the Airflow level:
- PARALLELISM (default=32) - defines the number of tasks that can be executed at the same time in the entire Airflow instance (all DAGs)
- DAG_CONCURENCY (default=16) - number of the tasks at the same time for given DAG
- MAX_ACTIVE_RUNS_PER_DAG  (default=16) - defines the number of dags runs that can be run at the same time for a given DAG

Concurrency - at the DAG level:
- `concurrency=2` - at most 2 tasks across all the DAG runs of a given DAG
- `max_active_runs=1` - how many DAG runs at the time

Concurrency - at the Task level:
- `task_concurrency=1` - number of task instances run at the same time for the whole DAG
- `pool='default_pool'`

### Dealing with resource consuming tasks with Pools

Pool - worker slots:
- slot is released after task competition
- Airflow -> Pool -> 'default_pool' = 128 slots
- without explicitly  specify task is running at 'default_pool'
- if the pool is full, the next task will be queued
- specific number of tasks that can be run at the same time for a specyfic set of tasks
- Adding new pool:
    - Airflow -> Pool -> Name, Number of Slots, Description
    - adding argument `pool='partner_pool'`
- argument `pool_slots=3` - can be used in any task - number of workers slots that given task takes - that one will take 3 slots (default=1)
- `pool` argument for SubDags won't be taken for all SubDags; it will be only taken for SubDagOperator task; has to be defined in each task of SubDag
 
### Execute critical tasks first, the others after

Task Priorities:

- `priority_weight=1` - if there are more tasks with the same priority, DAG will randomly pick one to proceed first
- bigger weights are run first
- priorities are evaluated at the pool level - has to be in the same pool
- manually triggered dags don't respect priorities
- `weight_rule` - parameter describing how, to sum up, weights of tasks in the pipeline
    - downstream - add up downstream (default)
    - upstream - add up on the upstream (opposite way around)
    - absolute - based on the priority rules you define (manually defined - won't change at all - it's full control)
- to prioritize a dag against another define a higher (99) priority for all dags tasks

### What if a task needs the output of its previous execution?

- `depends_on_past` - preventing from running next task if the task from the previous ds didn't succeed
- if the tasks will not be triggered will have no status (the previous task not done)
    - it is recommended to always have a timeout on dags otherwise will run forever
- for the first run, `depends_on_past` is ignored
- `depends_on_past` is taken into consideration if DAG is triggered manually
- the tasks are triggered even if the past task was skipped

### Demystifying wait for downstream

wait_for_downstream - commonly used with depends on past

- (1) [A] -> [B] -> [C]
- (2) [A] -> [B] -> [C]

if wait for downstream is True:
(2)[A] will run only if (1)[A] and (1)[B] succeded - only waiting for the direct downstream

- if you set this parameter, the `depends_on_past` will be automatically set to True
- might be useful when changing the same resources, to avoid race conditions

### All you need to know about Sensors

[Sensor](https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/sensors/index.html) - operator waiting something to happen (for condition to be true) before going to the next task
- eg. FileSensor, DateTimeSensor, SqlSensor
- Arguments:
    - `target_time` - specyfic for DateTimeSensor - `"{{ execution_date.add(hours=9) }}"`
    - `poke_interval=60` - frequency that sensor checks if condition is true or not (default=60seconds)
    - `mode='poke'` - worker slot is taken, better to use `mode='reschedule'` in order to release worker slot
    - `timeout=60 * 60 * 24 * 7` - waits fore this time (default=7days) for this sensor to complete
    - `soft_fail=True` - if `timeout` will be over it will skipp task
    - `execution_timeout` - available for all operators - max time allowed for the execution of this task instance, if it goes beyond it will raise and fail
    - `exponential_backoff=True` - next waiting in interval will be higher, usefull for http sensor in order no to ping server constantly 
- always define a timeout !!!

### Don't get stuck with your Tasks by using Timeouts

At the DAG level:
- can be defined at the DAG level with - `dagrun_timeout`
- always recommended to avoid having too many dags running indefinitely
- works only for scheduled dags - for manually triggered dags will not be used
At the Operator level:
- `execution_timeout=timedelta(minutes=10)`- doesn't have a value by default - the task will be failed if it will take longer than 10 minutes

The best practice is to define timeout on the DAG level and Operator level

### How to react in case of failure? or retry?

Callbacks:

- At the DAG level:
    - `on_success_callback=_some_success_python_function_callback`
    - `on_failure_callback=_some_failure_python_function_callback`
    - if there is a fail in the callback itselft it won't be retried automatically

```python
def _some_success_python_function_callback(context):
    print(context)
    # sending an email

def _some_failure_python_function_callback(context):
    print(context)
    # running some sql query and sending it's value
```

- At the Task level:
    - `on_success_callback=_some_success_python_function_callback`
    - `on_failure_callback=_some_failure_python_function_callback`
    - `on_retry_callback=_some_failure_python_function_callback`

```python
from airflow.exceptions import AirflowTaskTimeout, AirflowSensorTimeout
def _some_failure_python_function_callback(context):
    if (context['exception']):
        if (isinstance(context['exception'], AirflowTaskTimeout):
            # something
        if (isinstance(context['exception'], AirflowSensorTimeout):
            # something
        if (context['ti'].try_number() > 2 ):
            # something
    print ('FAILURE CALLBACK') 
    # running some sql query and sending it's value
```

### The different (and smart) ways of retrying your tasks

- `retries=0` - number of retries before task fails 
    - on the task level
    - in `default_args` - common for all of the operators
    - DEFAULT_TASK_RETRY - on the Airflow level
    - task level is more important than `default_args` level
- `retry_delay=timedelta(minutes=5)` - wait time between each retry
- `retry_exponential_backoff=True` 
    - will wait a little bit longer between each try
    - good for API or DB
- `max_retry_delay=timedelta(minutes=15)`- if `retry_exponential_backoff` will go over the limit it will eighter way retry (force) after this 15 minutes

### Get notified with SLAs

SLAs:
- if the task is taking longer than expected you want to get notifications
- to verify if the task is done in a given period of time

At the Task level:
- `sla=timedelta(minutes=5)` - sla is related to execution date of a DAG 
    - if the previous task will take this 5 minutes for completion, this one will miss it's sla (sum of time)
    - usually, it's ok to define this only for the last task

At the DAG level:
- `sla_miss_callback=_sla_miss_callback`
- there is no option to have different callbacks for each task - one for a whole DAG
- if the DAG is triggered manually SLA's won't be checked
- smtp email configuration in airflow has to be set to get notification
```python
def _sla_miss_callback(dag, task_list, blocking_task_list, slas, blocking_tis)
    print(task_list)
    print(blocking_tis)
    print(slas)
```
- task_list - corresponding task instance objects list of tasks
- blocking_task_list - t1 >> t2 - if both fail sla - the t1 will be blocking t2
- slas - list of all sls that missed their sls
- blocking_tis - it's the task_id which cased blocking

### DAG versioning

- there is no real mechanism to deal with this - it is coming in the future
- new tasks will have no status for the previous dag_runs
- for a queued task - the new version will not be taken into consideration
- for a removed task - all previous runs will not be showing anymore
- be very careful when
    - remove a task
    - a task is running during an update
- best practice - add version suffix _1_0_0

### Dynamic DAGs: The two ways!

The Single-File Method
- single Python file
- DAGs are generated every time the Scheduler parses the DAG folder
    - performance issues
- there is no way to see the code of the generated DAG on the UI
- if you change the number of generated DAGs, you will end up with DAGs that actually don't exist anymore on the UI
- easy to implement, but hard to maintain

The Multi-File Method
- the way to do it:
    - use a template file
    - create a script that will create a file of each generated DAG from the template
    - put the script for generating somewhere else than in a folder with DAGs
    - trigger the script manually or with CI/CD pipeline

- one Python file per generated DAG
- scalable - DAGs are not generated on the fly
- full visibility of the DAG code
- full control over the way DAGs are generated (script, less prone to errors or "zombie" DAGs)

## DAG dependencies

### Wait for multiple DAGs with the ExternalTaskSensor

ExternalTaskSensor - will wait for a task in another DAG to complete before moving forward

- dags should have the same execution date
- it's waiting for a task in a different DAG for a specific execution date

- arguments:
    - `external_dag_id`
    - `external_task_id`
    - `execution_delta` - use if the execution_times are different (time delta between execution of this DAG and another)
    - `execution_delta_fn` 
        - for more compelex cases 
        - expects a function  
        - possible to use context
        - can return multiple ds
    - `failed_states` - a list of statuses eg. ['failed', 'skipped']
    - `allowed_states` - eg. ['success']
- external sensor fails by default after 7 days
- if the task you are waiting for fails, the sensor task will wait until timeout

### DAG dependencies with the TriggerDagRunOperator

TriggerDagRunOperator - this can be a better way than the external Sensor

arguments:
- `task_id`
- `trigger_dag_id` - basically only task_id and trigger_dag_id is need
- `execution_date` - (string|datetime) - eg. "{{ ds }}"
- `wait_for_completion=True` - wait for the triggered dag to finish
- `poke_interval=60`- how often check if triggered DAG is completed
- `reset_dag_run=True` 
    - if you want to re-run the DAG, this tiggered one will give an error because there is no option of runnig the DAG if status is not cleaned
    - best to set this to True becasue it will automatically reset Triggered Dag
    - also backfill won't work
- `failed_states` 
    - a list of statuses eg. ['failed'] 
    - by default this list is empty 
    - always define it
- it's not based on sensor class - mode is not available!

### Notes

XCOMs are not removed automatically
